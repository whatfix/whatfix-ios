Pod::Spec.new do |s|

  s.name                        = "WhatFix"
  s.version                     = "1.0.22"
  s.summary                     = "Whatfix iOS SDK"
  s.homepage                    = "https://whatfix.com/"
  s.license                     = { :type => 'Commercial', :file => 'LICENSE', :text => 'See https://whatfix.com/terms' }
  s.author                      = { "Tarun Jain" => "tarun.jain@whatfix.com" }
  s.platform                    = :ios, "8.0"
  s.source                      = { :git => "https://bitbucket.org/whatfix/whatfix-ios.git", :tag => "v#{s.version}" }
  s.swift_version               = "5.2"
  s.ios.deployment_target       = '8.0'
  s.ios.vendored_frameworks     = 'Whatfix.xcframework'

end
