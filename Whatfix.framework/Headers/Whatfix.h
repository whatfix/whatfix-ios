//
//  Whatfix.h
//  Whatfix
//
//  Created by Tarun Jain on 03/07/20.
//  Copyright © 2020 Whatfix. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Whatfix.
FOUNDATION_EXPORT double WhatfixVersionNumber;

//! Project version string for Whatfix.
FOUNDATION_EXPORT const unsigned char WhatfixVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Whatfix/PublicHeader.h>
