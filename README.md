# Whatfix-iOS

Welcome to the official Whatfix iOS SDK

# Prerequisites

* iOS 8.0+
* Xcode 11+
* Objective-C / Swift 5

# Installation

## CocoaPods

[CocoaPods](https://cocoapods.org) is a dependency manager for Cocoa projects. For [usage and installation](https://guides.cocoapods.org/using/using-cocoapods.html) instructions, visit their website.

To integrate `Whatfix` into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
pod 'WhatFix', '~> 1.0.22'
```

Then, run the following command in the `terminal`:

```bash
$ pod install
```

## Manual

Start by downloading and decompressing the latest version of the [Whatfix iOS SDK](https://bitbucket.org/whatfix/whatfix-ios/get/master.zip)

### Add Whatfix XCframework to your project:

1. Open your project in Xcode.
2. Click on your project in the Project Navigator.
3. Select your target.
4. Make sure the General tab is selected.
5. Scroll down to `"Frameworks, Libraries, and Embedded Content"`.
6. Drag in `Whatfix.xcframework` from the downloaded SDK folder. It is wired up automatically as a dependency of your target.

![Whatfix Manual Install](https://drive.google.com/uc?id=10z8jMhJtRUHvM6j9VLGS8PKobUNql2tO&export=download)

### Verify Build Settings:

* `Enable Modules` should be set to `Yes`

    ![Whatfix Manual Install](https://drive.google.com/uc?id=1JmZmCiIlU56RD3e-iE9T--kjeq06AF3Z&export=download)

* `Link Frameworks Automatically` should be set to `Yes`

    ![Whatfix Manual Install](https://drive.google.com/uc?id=1hoq4IBiGReGvahFt7KMzkN24Is2gPgJS&export=download)

# Initializing and Usage

## Swift

Import Whatfix into `AppDelegate.swift`

```swift
import Whatfix
```

Initialize Whatfix at the end of `application:didFinishLaunchingWithOptions:`

```swift
Whatfix.initialize(entName: "<ent_name>", entId: "<ent_id>", application: application)
```

## Objective-C

Import Whatfix into `AppDelegate.m`

```objc
@import Whatfix;
```

Initialize Whatfix at the end of `application:didFinishLaunchingWithOptions:`

```objc
[Whatfix initializeWithEntName:@"<ent_name>" entId:@"<ent_id>" application:application];
```

Initialize your Whatfix instance with the `ent_name` and `ent_id` provided to you on whatfix.com dashboard.

# Other APIs:

**1.** Set your custom host to serve content. Default is `cdn.whatfix.com`

### Swift :
```swift
Whatfix.set(customContentLocation: "<custom_cdn_host>")
```
### Objective-C :
```objc
[Whatfix setWithCustomContentLocation:@"<custom_cdn_host>"];
```

**2.** Set the current user role from your app (e.g. `manager`)

### Swift :
```swift
Whatfix.set(loggedInUserRole: "<logged_in_user_role>")
```
### Objective-C :
```objc
[Whatfix setWithLoggedInUserRole:@"<logged_in_user_role>"];
```

**3.** Set the current user id for analytics (e.g. `john_doe`)

### Swift :
```swift
Whatfix.set(loggedInUserId: "<logged_in_user_id>")
```  
### Objective-C :
```objc
[Whatfix setWithLoggedInUserId:@"<logged_in_user_id>"];
```

**4.** Set the current user language preference (e.g. `fr`)

### Swift :
```swift
Whatfix.set(language: "<language_code>")
```
### Objective-C :
```objc
[Whatfix setWithLanguage:@"<language_code>"];
```

**5.** Set custom window variables for advanced segmentation (e.g. `["user_type":"paid"]`)

### Swift :
```swift
Whatfix.set(customKeyValue: ["user_type":"paid", "subscription_type":"monthly"])
```
### Objective-C :
```objc
[Whatfix setWithCustomKeyValue:@{@"user_type": @"paid", @"subscription_type": @"monthly"}];
```

**6.** Adding Margin/Padding to TaskList widget (e.g. `(left: 20, top: 0, right: 0, bottom: 20)`)

### Swift :
```swift
Whatfix.setTLMargin(left: 20, top: 0, right: 0, bottom: 20)
```
### Objective-C :
```objc
[Whatfix setTLMarginWithLeft:@20 top:@0 right:@0 bottom:@20];
```

# Advanced APIs:

### For other Advanced features please [refer](README_DEV.md).
