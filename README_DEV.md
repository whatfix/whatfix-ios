# Advanced APIs:

**1.** Enable Whatfix Debug Logs

### Swift :
```swift
Whatfix.isDebugEnabled = true
```
### Objective-C :
```objc
Whatfix.isDebugEnabled = TRUE;
```

**2.** Start Editor Directly

### Swift :
```swift
Whatfix.setEditorMode()
```
### Objective-C :
```objc
[Whatfix setEditorMode];
```

**3.** Disable Editor Mode

### Swift :
```swift
Whatfix.disableEditorMode()
```
### Objective-C :
```objc
[Whatfix disableEditorMode];
```